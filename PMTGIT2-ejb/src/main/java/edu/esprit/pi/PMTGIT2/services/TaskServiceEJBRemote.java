package edu.esprit.pi.PMTGIT2.services;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pi.PMTGIT2.persistence.Project;
import edu.esprit.pi.PMTGIT2.persistence.Task;


@Remote
public interface TaskServiceEJBRemote {
	public void addTask(Task t);
	public void addTaskToProject(int id, List<Task> listTasks);
	void updateTask(Task task);
	Boolean removeTask(Task tache);
	public List<Task> findAllTasks();
	public Task findTaskByName(String tname);
	public Task findTaskById(int idTask);
	
}
