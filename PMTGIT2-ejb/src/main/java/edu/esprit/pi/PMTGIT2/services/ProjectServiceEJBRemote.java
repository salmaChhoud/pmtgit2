package edu.esprit.pi.PMTGIT2.services;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pi.PMTGIT2.persistence.Project;







@Remote
public interface ProjectServiceEJBRemote {
	public void addProject(Project p);

	public void deleteProject(Project p);
	public void updateProject(Project p);
	public Project  findProjectById(int id);
	public List<Project> findAll();
	
}
